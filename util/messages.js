module.exports = {
    file: {
        saved: 'File saved successfully',
        error: 'There was a problem saving the file'
    }
}