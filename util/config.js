const VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');

module.exports = {
    watson: {
        conversation:{
            username: '7dcce03f-f49d-4f07-ae08-f0d1e6ddc2b0',
            password: '5lETWaZRTvnt',
            workspace_id: '88afff51-76fe-4971-a39b-cbd3e031c3d1',
            version: 'v1',
            version_date: '2017-05-26'
        },
        visual_recognition:{
            classifier_ids: '',
            api_key: '',
            version_date: VisualRecognitionV3.VERSION_DATE_2016_05_20
        }
    }
}