const express = require('express');
const morgan = require('morgan');
const watson = require('watson-developer-cloud');
const bodyParser = require('body-parser');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const request = require('request');

//const config = require('./util/config');
var config;
var conversation;
var visual_recognition;
const messages = require('./util/messages');

const tempDir = path.join(__dirname, 'temp');

var upload = multer({storage: multer.memoryStorage()});

var app = express();
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function loadConfig() {
    request('https://ztwatsonconfig.mybluemix.net/config', (err, res, body) => {
        config = JSON.parse(body);
        conversation = watson.conversation({
            username: config.watson.conversation.username,
            password: config.watson.conversation.password,
            version: config.watson.conversation.version,
            version_date: config.watson.conversation.version_date
        });
        console.log('Conversation service initialized');
        visual_recognition = new watson.VisualRecognitionV3({
            api_key: config.watson.visual_recognition.api_key,
            version_date: config.watson.visual_recognition.version_date
        });
        console.log('Visual recognition service initialized');
        config.lastReload = new Date();
    });
}

// GET /conversation/initialize
app.get('/conversation/initialize', (req, res) => {
    conversation.message({
        workspace_id: config.watson.conversation.workspace_id,
        input: {'text': 'Hello'}
    }, (err, response) => {
        if(err) {
            console.log('error', err);
        } else {
            res.send(response);
        }
    });
});

// POST /conversation/message/media/:conversation_id
app.post('/conversation/message/media/:conversation_id', upload.single('image'), (req, res) => {
    var tempImage = tempDir + '/' + req.file.originalname;
    fs.writeFile(tempImage, new Buffer(req.file.buffer, "base64"), (err) => {
        //include proper settings for debug
        //console.log(messages.file.saved);
        var params = {
            images_file: fs.createReadStream(tempImage),
            classifier_ids: config.watson.visual_recognition.classifier_ids
        };

        visual_recognition.classify(params, (err, vr_response) => {
            if(err) {
                console.log(err);
                res.send(err);
            } else {
                console.log(vr_response);
                //perform check on the image response, and check the confidence level, get category and begin conversation piece using that ->
                //may need to think through the dialog tree a lot more, should we pass an image parameter to the service so it knows how to respond?

                conversation.message({
                    workspace_id: config.watson.conversation.workspace_id,
                    input: {'text' : ''}
                    ///TODO: add conversation_id
                }, (err, conv_response) => {
                    //pass conversation response back to the caller with context
                    //call out to commerce for products = response
                });
            }
        });
    });

});

// POST /conversation/message/text/:conversation_id
app.post('/conversation/message/text/:conversation_id', (req, res) => {
    res.send('Text received, method tbd');
});

app.get('/config/current', (req, res) => {
    res.send(config);
});

app.get('/config/reload', (req, res) => {
    loadConfig();
    res.send('Reload request received');
});

app.listen(process.env.PORT || 3000, (err) => {
    loadConfig();
    console.log('Listening on port: ', process.env.PORT || 3000);
});